import AbstractService from "./AbstractService";

export default class ColorService extends AbstractService
{
    getColor(id)
    {
        return this.get(`/colors/${id}`);
    }

    getColors()
    {
        return this.get('/colors');
    }

    addColor(colorData)
    {
        return this.post('/colors/add/', colorData)
    }

    updateColor(id, data)
    {
        return this.patch(`/colors/${id}/update/`, data)
    }

    deleteColor(id)
    {
        return this.delete(`/colors/${id}/delete/`)
    }
}