export default class AbstractService
{
    constructor() {
        this.baseURL = 'http://localhost:9000';
    }

    get(url) {
        return fetch(this.baseURL + url);
    }

    post(url, data) {
        return fetch(this.baseURL + url, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(data)
        });
    }

    patch(url, data) {
        return fetch(this.baseURL + url, {
            method: 'PATCH',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(data)
        });
    }

    delete(url){
        return fetch(this.baseURL + url, {
            method: 'DELETE'
        })
    }
}