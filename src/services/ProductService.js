import AbstractService from "./AbstractService";

export default class ProductService extends AbstractService
{
    getProduct(id)
    {
        return this.get(`/products/${id}`);
    }

    getProducts()
    {
        return this.get('/products');
    }

    addProduct(productData)
    {
        return this.post('/products/add/', productData)
    }

    updateProduct(id, data)
    {
        return this.patch(`/products/${id}/update/`, data)
    }

    deleteProduct(id)
    {
        return this.delete(`/products/${id}/delete/`)
    }
}
