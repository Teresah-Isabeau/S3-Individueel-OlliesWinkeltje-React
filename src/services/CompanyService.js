import AbstractService from "./AbstractService";

export default class CompanyService extends AbstractService
{
    getCompany(id)
    {
        return this.get(`/companys/${id}`);
    }

    getCompanys()
    {
        return this.get('/companys');
    }

    addCompany(companyData)
    {
        return this.post('/companys/add/', companyData)
    }

    updateCompany(id, data)
    {
        return this.patch(`/companys/${id}/update/`, data)
    }

    deleteCompany(id)
    {
        return this.delete(`/companys/${id}/delete/`)
    }
}