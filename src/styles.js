import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
    toolbar: theme.mixins.toolbar,
    content: {
        // flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(10),
    },
    root: {
        // flexGrow: 1,
        // maxWidth: '100%'
    },

    // media: {
    //     height: 0,
    //     paddingTop: '56.25%', // 16:9
    // },
    // tableActions: {
    //     display: 'flex',
    //     justifyContent: 'flex-end',
    // },
    // tableContent: {
    //     display: 'flex',
    //     justifyContent: 'space-between',
    // }
}));