import React, {useEffect, useState} from "react";
import {Button, Grid} from "@material-ui/core";

import Product from "../Product/Product";
import ProductService from "../../services/ProductService";

const Products = ({onAddToCart}) => {
    const [products, setProducts] = useState([]);

    const fetchProducts = () => {
        const productService = new ProductService();
        return productService.getProducts();
    }

    useEffect(() => {
        fetchProducts()
            .then((r) => r.json())
            .then((products) => {
                setProducts(products);
            });
        // fetchCart();
    }, []);

    if (products.length === 0) {
        return(
            <h3>Loading...</h3>
        )}
    return(
        <div>
            <Grid container justifyContent="center" spacing={4}>
                {products.map((product) => (
                    <Grid item key={product.id} xs={12} sm={6} md={4} lg={3}>
                        <Product product={product} onAddToCart={onAddToCart}/>
                    </Grid>
                ))}
            </Grid>
            <Button variant="contained" style={{float: 'right'}} href={'/chat'}>
                Help
            </Button>
        </div>
    );
}

export default Products;