import React from "react";
import {Card, CardMedia, CardContent, CardActions, Typography, IconButton, Button} from "@material-ui/core";
import {AddShoppingCart} from "@material-ui/icons";
import useStyles from './styles';

const Product = ({product, onAddToCart}) => {
    const  classes = useStyles();

    const handleAddToCart = () => onAddToCart(product.id, 1);

    return(
        <div>
            <Card className={classes.root}>
                <CardMedia className={classes.media} image='' title={product.name} />
                <CardContent>
                    <div className={classes.cardContent}>
                        <Typography variant="h5" gutterBottom>
                            {product.name}
                        </Typography>
                        <Typography gutterBottom variant="h5" component="h2">
                            €{product.price / 100}
                        </Typography>
                    </div>
                    <Typography dangerouslySetInnerHTML={{ __html: product.description }} variant="body2" color="textSecondary" component="p" />
                </CardContent>
                <CardActions disableSpacing className={classes.cardActions}>
                    <IconButton aria-label="Add to cart" onClick={handleAddToCart}>
                        <AddShoppingCart />
                    </IconButton>
                </CardActions>
            </Card>
        </div>
    );
};

export default Product;