import React, {useState} from "react";
import ColorService from "../../../services/ColorService";
import {Button} from "@material-ui/core";
import Table from '@mui/material/Table';
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import TableHead from "@mui/material/TableHead";
import TableBody from "@mui/material/TableBody";
import PropTypes from "prop-types";
import TableContainer from "@mui/material/TableContainer";
import Paper from "@mui/material/Paper";
import _ from "underscore";

const AdminColors = () => {
    const [colors, setColors] = useState([]);

    const fetchColors = () => {
        const colorService = new ColorService();
        colorService.getColors().then((r) => r.json())
            .then((colors) => {
                setColors(colors);
            });
    }

    useState(() => {
        fetchColors()
    })

    const deleteColor = (id) => {
        const colorService = new ColorService();
        colorService.deleteColor(id).then(() => {
            fetchColors()
        });
    }

    if (colors.length === 0) {
        return (
            <h3>Loading...</h3>
        )
    }

    function Row(data) {
        const color = data.row;

        return (
            <React.Fragment>
                <TableRow sx={{'& > *': {borderBottom: 'unset'}}}>
                    <TableCell component="th" scope="row">{color.id}</TableCell>
                    <TableCell align="left">{color.name}</TableCell>
                    <TableCell>
                        <Button variant="contained" style={{backgroundColor:'#ffc107'}} href={`/admin/colors/${color.id}/update`}>
                            Edit
                        </Button>
                        <Button variant="contained" style={{marginLeft: 10, backgroundColor: '#dc3545'}} onClick={() => deleteColor(color.id)}>
                            Delete
                        </Button>
                    </TableCell>
                </TableRow>
            </React.Fragment>
        );
    }

    Row.propTypes = {
        colors: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.number.isRequired,
            name: PropTypes.string.isRequired,
        }).isRequired)
    };

    return (
        <div>
            <div>
                <Button variant="contained" style={{float: 'left'}} href="/admin/">
                    Back
                </Button>
                <Button variant="contained" style={{float: 'right'}} href="/admin/colors/add">
                    Create color
                </Button>
            </div>
            <div>
                <TableContainer component={Paper} style={{}}>
                    <Table aria-label="collapsible table">
                        <TableHead>
                            <TableRow>
                                <TableCell align="left">Id</TableCell>
                                <TableCell align="left">Name</TableCell>
                                <TableCell align="left">Actions</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {_.map(colors, (color) => (
                                <Row key={color.id} row={color}/>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </div>
        </div>
    );
}

export default AdminColors;