import {Button, CardActions, CardMedia, CssBaseline, Grid} from "@material-ui/core";
import {Link} from "react-router-dom";
import {Card, CardActionArea , CardContent, Typography} from "@material-ui/core";
import useStyles from './styles';

const AdminIndexPage = () => {
    const  classes = useStyles();
    const AdminLinks = [
        {
            name: 'Products',
            to: '/admin/products'
        },
        {
            name: 'Colors',
            to: '/admin/colors'
        },
        {
            name: 'Companies',
            to: '/admin/companys'
        }
    ]

    return(
        <div>
            <div className={'card-outer'}>
                <Card>
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="div">
                            Products
                        </Typography>
                        <Typography variant="body2" color="text.secondary">
                            Manage your products
                        </Typography>
                    </CardContent>
                    <CardActions>
                        <Button size="small" href={'/admin/products'}>Overview</Button>
                    </CardActions>
                </Card>
            </div>

            <div className={'card-outer'}>
                <Card>
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="div">
                            Colors
                        </Typography>
                        <Typography variant="body2" color="text.secondary">
                            Manage your colors
                        </Typography>
                    </CardContent>
                    <CardActions>
                        <Button size="small" href={'/admin/colors'}>Overview</Button>
                    </CardActions>
                </Card>
            </div>

            <div className={'card-outer'}>
                <Card>
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="div">
                            Companies
                        </Typography>
                        <Typography variant="body2" color="text.secondary">
                            Manage your companies
                        </Typography>
                    </CardContent>
                    <CardActions>
                        <Button size="small" href={'/admin/companys'}>Overview</Button>
                    </CardActions>
                </Card>
            </div>


        </div>
    );
}

export default AdminIndexPage;
