import {  Button, Grid, Typography } from "@material-ui/core";
import { useForm, FormProvider } from "react-hook-form";
import { useNavigate } from "react-router-dom";

import { Link } from "react-router-dom";
import FormInput from "../CustomTextField";
import CompanyService from "../../../services/CompanyService";


const CreateCompany = () => {
    const methods = useForm();
    const navigate = useNavigate();

    const onSubmit = async data => {

        const companyData = {
            company: {
                name: data.name
            }
        }
        const companyService = new CompanyService();
        await companyService.addCompany(companyData).then((response) => {
            if (response.status !== 200) {
                throw Error(response.statusText)
            }
            return response.json()
        })
        .then((company) => {
            navigate("/admin/companys");
        }).catch(error => {
            alert(error.message)
        })
    };

    return (
        <>
            <Typography variant="h6" gutterBottom>Create new company</Typography>
            <FormProvider {...methods}>
                <form onSubmit={methods.handleSubmit(onSubmit)}>
                    <Grid container spacing={3}>
                        <FormInput required name="name" label="Name" />
                    </Grid>
                    <br />
                    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                        <Button component={Link} variant="outlined" to="/admin/companys">Back to companies</Button>
                        <Button type="submit" variant="contained" color="primary">Add</Button>
                    </div>
                </form>
            </FormProvider>
        </>
    );
};

export default CreateCompany;