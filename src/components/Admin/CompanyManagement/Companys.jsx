import React, {useState} from "react";
import CompanyService from "../../../services/CompanyService";
import {Button} from "@material-ui/core";
import Table from '@mui/material/Table';
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import PropTypes from "prop-types";
import TableContainer from "@mui/material/TableContainer";
import Paper from "@mui/material/Paper";
import TableHead from "@mui/material/TableHead";
import TableBody from "@mui/material/TableBody";
import _ from "underscore";

const AdminCompany = () => {
    const [companys, setCompanys] = useState([]);

    const fetchCompanys = () => {
        const companyService = new CompanyService();
        companyService.getCompanys().then((r) => r.json())
        .then((companys) => {
            setCompanys(companys);
        });
    }

    useState(() => {
        fetchCompanys()
    })

    const deleteCompany = (id) => {
        const companyService = new CompanyService();
        companyService.deleteCompany(id).then(() => {
            fetchCompanys()
        });
    }

    if(companys.length === 0){
        return(
            <h3>Loading...</h3>
        )
    }

    function Row(data) {
        const company = data.row;

        return (
            <React.Fragment>
                <TableRow sx={{'& > *': {borderBottom: 'unset'}}}>
                    <TableCell component="th" scope="row">{company.id}</TableCell>
                    <TableCell align="left">{company.name}</TableCell>
                    <TableCell>
                        <Button variant="contained" style={{backgroundColor:'#ffc107'}} href={`/admin/companys/${company.id}/update`}>
                            Edit
                        </Button>
                        <Button variant="contained" style={{marginLeft: 10, backgroundColor: '#dc3545'}} onClick={() => deleteCompany(company.id)}>
                            Delete
                        </Button>
                    </TableCell>
                </TableRow>
            </React.Fragment>
        );
    }

    Row.propTypes = {
        companys: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.number.isRequired,
            name: PropTypes.string.isRequired,
        }).isRequired)
    };

    return (
        <div>
            <Button variant="contained" style={{float: 'left'}} href="/admin/">
                Back
            </Button>
            <Button variant="contained" style={{float: 'right'}} href="/admin/companys/add">
                Create company
            </Button>
            <TableContainer component={Paper}>
                <Table aria-label="collapsible table">
                    <TableHead>
                        <TableRow>
                            <TableCell align="left">Id</TableCell>
                            <TableCell align="left">Name</TableCell>
                            <TableCell align="left">Actions</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {_.map(companys, (company) => (
                            <Row key={company.id} row={company}/>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    );
}

export default AdminCompany;