import {FormProvider, useForm} from "react-hook-form";
import ProductService from "../../../services/ProductService";
import {Link, useNavigate} from "react-router-dom";
import {Button, Grid, InputLabel, MenuItem, Select, Typography} from "@material-ui/core";
import FormInput from "../CustomTextField";
import _ from "underscore";
import React, {useEffect, useState} from "react";
import ColorService from "../../../services/ColorService";
import CompanyService from "../../../services/CompanyService";
import { useParams } from 'react-router-dom';


const UpdateProduct = () => {
    const methods = useForm();
    const { productId } = useParams();
    const navigate = useNavigate();
    const [product, setProduct] = useState(null);
    const [colors, setColors] = useState([]);
    const [companys, setCompanys] = useState([]);
    const [selectedCompany, setSelectedCompany] = useState(null);
    const [selectedColor, setSelectedColor] = useState(null);

    const fetchProduct = () => {
        const productService = new ProductService();
        return productService.getProduct(productId);
    }

    useEffect(() => {
        fetchProduct()
            .then((r) => r.json())
            .then((product) => {
                setProduct(product);
                setSelectedColor(product.color.id);
                setSelectedCompany(product.company.id);
            });
    }, []);

    const fetchColors = () => {
        const webShopApi = new ColorService();
        return webShopApi.getColors();
    };

    const fetchCompanys = () => {
        const webShopApi = new CompanyService();
        return webShopApi.getCompanys();
    };

    useState(() => {
        fetchColors()
            .then((r) => r.json())
            .then((colors) => {
                setColors(colors);
            })
    });

    useState(() => {
        fetchCompanys()
            .then((r) => r.json())
            .then((companys) => {
                console.info(companys)
                setCompanys(companys);
            })
    });

    const onSubmit = async data => {
        const productData = {
            product: {
                name: data.name,
                description: data.name,
                productCode: data.productCode,
                stock: data.stock,
                price: data.price,
                appearanceYear: data.appearanceYear,
                color: selectedColor,
                company: selectedCompany
            }
        }
        const productService = new ProductService();
        await productService.updateProduct(productId, productData).then((response) => {
            if (response.status !== 200) {
                throw Error(response.statusText)
            }
            return response.json()
        })
            .then((product) => {
                navigate("/admin/products");
            }).catch(error => {
                alert(error.message)
            })
        };

        if (!product || colors.length === 0 || companys.length === 0){
            return(
                <h3>Loading...</h3>
            )
        }

    return (
        <>
            <Typography variant="h6" gutterBottom>Create new product</Typography>
            <FormProvider {...methods}>
                <form onSubmit={methods.handleSubmit(onSubmit)}>
                    <Grid container spacing={3}>
                        <FormInput required name="name" label="Name" defaultValue={product.name} />
                        <FormInput required name="productCode" label="Product code" type={'number'} defaultValue={product.product_code} />
                        <FormInput required name="stock" label="Stock" type={'number'} defaultValue={product.stock} />
                        <FormInput required name="price" label="Price in cents" type={'number'} defaultValue={product.price} />
                        <FormInput required name="appearanceYear" label="Year of appearance" defaultValue={product.appearance_year} />
                        <br />
                        <FormInput required name="description" label="description" defaultValue={product.description}/>
                        <Grid item xs={12} sm={6}>
                            <InputLabel>Select color</InputLabel>
                            <Select value={selectedColor} fullWidth onChange={(e) => setSelectedColor(e.target.value)}>
                                {_.map(colors, (color) => {
                                    return (
                                        <MenuItem key={color.id} value={color.id}>
                                            {color.name}
                                        </MenuItem>
                                    )
                                })}
                            </Select>
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <InputLabel>Select company</InputLabel>
                            <Select value={selectedCompany} fullWidth onChange={(e) => setSelectedCompany(e.target.value)}>
                                {_.map(companys, (company) => {
                                    return (
                                        <MenuItem key={company.id} value={company.id}>
                                            {company.name}
                                        </MenuItem>
                                    )
                                })}
                            </Select>
                        </Grid>
                    </Grid>
                    <br />
                    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                        <Button component={Link} variant="outlined" to="/admin/products">Back to products</Button>
                        <Button type="submit" variant="contained" color="primary">Update</Button>
                    </div>
                </form>
            </FormProvider>
        </>
    );
};

export default UpdateProduct;