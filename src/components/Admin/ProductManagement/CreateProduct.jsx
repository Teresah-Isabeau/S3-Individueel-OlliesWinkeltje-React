import React, { useState } from "react";
import { InputLabel, Select, MenuItem, Button, Grid, Typography } from "@material-ui/core";
import { useForm, FormProvider} from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";
import _ from 'underscore';
import FormInput from "../CustomTextField";
import CompanyService from "../../../services/CompanyService";
import ColorService from "../../../services/ColorService";
import ProductService from "../../../services/ProductService";

const CreateProduct = () => {
    const methods = useForm();
    const navigate = useNavigate();

    const onSubmit = async data => {

        const productData = {
            product: {
                name: data.name,
                description: data.name,
                productCode: data.productCode,
                stock: data.stock,
                price: data.price,
                appearanceYear: data.appearanceYear,
                color: selectedColor,
                company: selectedCompany
            }
        }
        const productService = new ProductService();
        await productService.addProduct(productData).then((response) => {
            if (response.status !== 200) {
                throw Error(response.statusText)
            }
            return response.json()
        })
        .then((product) => {
            navigate("/admin/products");
        }).catch(error => {
            alert(error.message)
        })
    };

    const [colors, setColors] = useState([]);
    const [companys, setCompanys] = useState([]);
    const [selectedCompany, setSelectedCompany] = useState(null);
    const [selectedColor, setSelectedColor] = useState(null);

    const fetchColors = () => {
        const webShopApi = new ColorService();
        return webShopApi.getColors();
    }

    const fetchCompanys = () => {
        const webShopApi = new CompanyService();
        return webShopApi.getCompanys();
    }

    useState(() => {
        fetchColors()
            .then((r) => r.json())
            .then((colors) => {
                setColors(colors);
            })
    })

    useState(() => {
        fetchCompanys()
            .then((r) => r.json())
            .then((companys) => {
                console.info(companys)
                setCompanys(companys);
            })
    })

    if(colors.length === 0 || companys.length === 0){
        return(
            <h3>Loading...</h3>
        )
    }

    return (
        <>
            <Typography variant="h6" gutterBottom>Create new product</Typography>
            <FormProvider {...methods}>
                <form onSubmit={methods.handleSubmit(onSubmit)}>
                    <Grid container spacing={3}>
                        <FormInput required name="name" label="Name" />
                        <FormInput required name="productCode" label="Product code" type={'number'} />
                        <FormInput required name="stock" label="Stock" type={'number'} />
                        <FormInput required name="price" label="Price in cents" type={'number'} />
                        <FormInput required name="appearanceYear" label="Year of appearance" />
                        <br />
                        <FormInput required name="description" label="description" />
                        <Grid item xs={12} sm={6}>
                            <InputLabel>Select color</InputLabel>
                            <Select value={selectedColor} fullWidth onChange={(e) => setSelectedColor(e.target.value)}>
                                {_.map(colors, (color) => {
                                    return (
                                        <MenuItem key={color.id} value={color.id}>
                                            {color.name}
                                        </MenuItem>
                                    )
                                })}
                            </Select>
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <InputLabel>Select company</InputLabel>
                            <Select value={selectedCompany} fullWidth onChange={(e) => setSelectedCompany(e.target.value)}>
                                {_.map(companys, (company) => {
                                return (
                                    <MenuItem key={company.id} value={company.id}>
                                        {company.name}
                                    </MenuItem>
                                    )
                                })}
                            </Select>
                        </Grid>
                    </Grid>
                    <br />
                    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                        <Button component={Link} variant="outlined" to="/admin/products">Back to products</Button>
                        <Button type="submit" variant="contained" color="primary">Add</Button>
                    </div>
                </form>
            </FormProvider>
        </>
    );
};

export default CreateProduct;
