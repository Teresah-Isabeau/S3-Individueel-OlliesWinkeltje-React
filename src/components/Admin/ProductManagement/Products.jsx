import React, { useState} from "react";
import _ from 'underscore';
import ProductService from "../../../services/ProductService";
import {Button} from "@material-ui/core";
import PropTypes from 'prop-types';
import Box from '@mui/material/Box';
import Collapse from '@mui/material/Collapse';
import IconButton from '@mui/material/IconButton';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';

const AdminProducts = () => {
    const [products, setProducts] = useState([]);

    const fetchProducts = () => {
        const productService = new ProductService();
        productService.getProducts().then((r) => r.json())
        .then((products) => {
            setProducts(products);
        });
    }

    useState(() => {
        fetchProducts()
    })

    const deleteProduct = (id) => {
        const productService = new ProductService();
        productService.deleteProduct(id).then(()=>{
            fetchProducts()
        });
    }

    if(products.length === 0){
        return(
            <h3>Loading...</h3>
        )
    }

    function Row(data) {
        const product = data.row;

        const [open, setOpen] = React.useState(false);
        return (
            <React.Fragment>
                <TableRow sx={{ '& > *': { borderBottom: 'unset' } }}>
                    <TableCell>
                        <IconButton
                            aria-label="expand row"
                            size="small"
                            onClick={() => setOpen(!open)}
                        >
                            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                        </IconButton>
                    </TableCell>
                    <TableCell component="th" scope="row">{product.id}</TableCell>
                    <TableCell align="left">{product.name}</TableCell>
                    <TableCell align="left">{product.stock}</TableCell>
                    <TableCell align="left">{product.price}</TableCell>
                    <TableCell>
                        <Button variant="contained" style={{backgroundColor:'#ffc107'}} href={`/admin/products/${product.id}/update`}>
                            Edit
                        </Button>
                        <Button variant="contained" style={{marginLeft: 10, backgroundColor: '#dc3545'}} onClick={() => deleteProduct(product.id)}>
                            Delete
                        </Button>
                    </TableCell>
                </TableRow>
                <TableRow>
                    <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                        <Collapse in={open} timeout="auto" unmountOnExit>
                            <Box sx={{ margin: 1 }}>
                                <Typography variant="h6" gutterBottom component="div">
                                    {/*Details*/}
                                </Typography>
                                <Table size="small" aria-label="purchases">
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Appearance year</TableCell>
                                            <TableCell>Color</TableCell>
                                            <TableCell align="left">Company</TableCell>
                                            <TableCell align="left">Description</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        <TableRow key={product.id}>
                                            <TableCell component="th" scope="row">{product.appearance_year}</TableCell>
                                            <TableCell align="left">{product.color.name}</TableCell>
                                            <TableCell align="left">{product.company.name}</TableCell>
                                            <TableCell align="left">{product.description}</TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>
                            </Box>
                        </Collapse>
                    </TableCell>
                </TableRow>
            </React.Fragment>
        );
    }

    Row.propTypes = {
        products: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.number.isRequired,
            name: PropTypes.string.isRequired,
            stock: PropTypes.number.isRequired,
            price: PropTypes.number.isRequired,
            appearance_year: PropTypes.string.isRequired,
            color: PropTypes.string.isRequired,
            company: PropTypes.string.isRequired,
            description: PropTypes.string.isRequired,
        }).isRequired)
    };

    return(
        <div>
            <Button variant="contained" style={{float: 'left'}} href="/admin/">
                Back
            </Button>
            <Button variant="contained" style={{float: 'right'}} href="/admin/products/add">
                Create product
            </Button>
            <TableContainer component={Paper}>
                <Table aria-label="collapsible table">
                    <TableHead>
                        <TableRow>
                            <TableCell />
                            <TableCell align="left">Id</TableCell>
                            <TableCell align="left">Name</TableCell>
                            <TableCell align="left">Stock</TableCell>
                            <TableCell align="left">Price</TableCell>
                            <TableCell align="left">Actions</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {_.map(products, (product) => (
                            <Row key={product.id} row={product}/>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    );
}

export default AdminProducts;
