import React from 'react';
import { useFormContext, Controller } from 'react-hook-form';
import { TextField, Grid } from '@material-ui/core';

function FormInput({ name, label, required, defaultValue, type='text' }) {
    const { control, register } = useFormContext();
    const isError = false;

    return (
        <Grid item xs={12} sm={6}>
            <Controller
                as={TextField}
                control={control}
                name={name}
                label={label}
                error={isError}
                render = {({ field})=> (
                    <TextField
                        type={type}
                        fullWidth
                        label={label}
                        required
                        defaultValue={defaultValue}
                        {...register(name)}
                    />
                )}
            />
        </Grid>
    );
}

export default FormInput;