import React, { useState, useEffect, useRef } from 'react';
import { HubConnectionBuilder } from '@microsoft/signalr';

import ChatWindow from "./ChatWindow";
import ChatInput from "./ChatInput";
import {Button, Grid} from "@material-ui/core";
import Product from "../Product/Product";

const Chat = () => {
    const [ connection, setConnection ] = useState(null);
    const [ chat, setChat ] = useState([]);
    const latestChat = useRef(null);

    latestChat.current = chat;

    useEffect(() => {
        const newConnection = new HubConnectionBuilder()
            // .withUrl('http://localhost:9001/chat')
            .withUrl('https://localhost:44367/chat')
            .withAutomaticReconnect()
            .build();

        setConnection(newConnection);
    }, []);

    useEffect(() => {
        if (connection) {
            connection.start()
                .then(result => {
                    console.log('Connected!');

                    connection.on('ReceiveMessage', message => {
                        const updatedChat = [...latestChat.current];
                        updatedChat.push(message);

                        setChat(updatedChat);
                    });
                })
                .catch(e => console.log('Connection failed: ', e));
        }
    }, [connection]);

    const sendMessage = async (user, message) => {
        const chatMessage = {
            user: user,
            message: message
        };

        if (connection._connectionStarted) {
            try {
                await connection.send('SendMessage', chatMessage);
            }
            catch(e) {
                console.log(e);
            }
        }
        else {
            alert('No connection to server yet.');
        }
    }

    return (
        <div>
            <div>
                <Button variant="contained" style={{float: 'left'}} href="/">
                    Back
                </Button>
            </div>
            <div>
                <Grid container justifyContent="center" spacing={4}>
                    <Grid item xs={12} sm={6} md={4} lg={6}>
                        <ChatInput sendMessage={sendMessage} />
                        <hr />
                        <ChatWindow chat={chat}/>
                    </Grid>
                </Grid>

            </div>
        </div>
    );
};

export default Chat;
