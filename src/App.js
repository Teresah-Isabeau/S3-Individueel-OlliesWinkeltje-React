import React, { useState, useEffect } from "react";
import { CssBaseline } from '@material-ui/core';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

import ProductService from "./services/ProductService";
import { Products, Navbar, Cart, CheckOut } from "./components";
import AdminIndexPage from "./components/Admin/AdminIndexPage";
import AdminProducts from "./components/Admin/ProductManagement/Products";
import AdminColors from "./components/Admin/ColorManagement/Colors";
import AdminCompany from "./components/Admin/CompanyManagement/Companys";
import useStyles from "./styles";
import CreateProduct from "./components/Admin/ProductManagement/CreateProduct";
import CreateColor from "./components/Admin/ColorManagement/CreateColor";
import CreateCompany from "./components/Admin/CompanyManagement/CreateCompany";
import UpdateProduct from "./components/Admin/ProductManagement/UpdateProduct";
import Chat from "./components/Chat/Chat";

const App = () =>{
    const classes = useStyles();
    const [mobileOpen, setMobileOpen] = React.useState(false);
    // const [products, setProducts] = useState([]);
    const [cart, setCart] = useState({});
    // const [order, setOrder] = useState({});
    // const [errorMessage, setErrorMessage] = useState('');

    // const fetchProducts = async () => {
    //     const webShopApi = new WebShopApi();
    //     const {data} = await webShopApi.getProducts();
    //
    //     setProducts(data);
    // }

    // const fetchCart = async () => {
    //     const cart = await WebShopApi.cart.retrieve();
    //
    //     setCart(cart);
    // }
    //
    // const handleAddToCart = async (productId, quantity) => {
    //     const item = await WebShopApi.cart.add(productId, quantity);
    //
    //     setCart(item.cart);
    // }
    //
    // const handleUpdateCartQty = async (lineItemId, quantity) => {
    //     const response = await WebShopApi.cart.update(lineItemId, { quantity });
    //
    //     setCart(response.cart);
    // };
    //
    // const handleRemoveFromCart = async (lineItemId) => {
    //     const response = await WebShopApi.cart.remove(lineItemId);
    //
    //     setCart(response.cart);
    // };
    //
    // const handleEmptyCart = async () => {
    //     const response = await WebShopApi.cart.empty();
    //
    //     setCart(response.cart);
    // };
    //
    // const refreshCart = async () => {
    //     const newCart = await WebShopApi.cart.refresh();
    //
    //     setCart(newCart);
    // };
    //
    // const handleCaptureCheckout = async (checkoutTokenId, newOrder) => {
    //     try {
    //         const incomingOrder = await WebShopApi.checkout.capture(checkoutTokenId, newOrder);
    //
    //         setOrder(incomingOrder);
    //
    //         refreshCart();
    //     } catch (error) {
    //         setErrorMessage(error.data.error.message);
    //     }
    // };

    // useEffect(() => {
    //      fetchProducts();
    //     // fetchCart();
    // }, []);
    // console.log(products);

    const handleDrawerToggle = () => setMobileOpen(!mobileOpen);


    return(
    <Router>
        <div>
            <CssBaseline />
            <Navbar totalItems={cart.total_items} handleDrawerToggle={handleDrawerToggle} />
            <main className={classes.content}>
                <div className={classes.toolbar}>
                    <Routes>
                        <Route exact path="/" element={<Products />}>
                            {/*<Products products={products} onAddToCart={handleAddToCart} handleUpdateCartQty />*/}
                        </Route>
                        <Route exact path="/cart" element={<Cart />}>
                            {/*<Cart cart={cart} onUpdateCartQty={handleUpdateCartQty} onRemoveFromCart={handleRemoveFromCart} onEmptyCart={handleEmptyCart} />*/}
                        </Route>
                        <Route path="/checkout" exact element={<CheckOut />}>
                            {/*<CheckOut cart={cart} order={order} onCaptureCheckout={handleCaptureCheckout} error={errorMessage} />*/}
                        </Route>
                        <Route path="/chat" exact element={<Chat />}>
                        </Route>
                        <Route path="/admin" exact element={<AdminIndexPage />}>
                        </Route>
                        <Route exact path="/admin/products" element={<AdminProducts />}>
                        </Route>
                        <Route exact path="/admin/products/add" element={<CreateProduct />}>
                        </Route>
                        <Route exact path="/admin/products/:productId/update" element={<UpdateProduct />}>
                        </Route>
                        <Route exact path="/admin/colors" element={<AdminColors />}>
                        </Route>
                        <Route exact path="/admin/colors/add" element={<CreateColor />}>
                        </Route>
                        <Route path="/admin/companys" exact element={<AdminCompany />}>
                        </Route>
                        <Route path="/admin/companys/add" exact element={<CreateCompany />}>
                        </Route>
                    </Routes>
                </div>
            </main>
        </div>
    </Router>
    );
};

export default App;